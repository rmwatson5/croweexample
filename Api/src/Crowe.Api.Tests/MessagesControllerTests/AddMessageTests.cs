﻿using System.Collections.Generic;
using System.Linq;
using Crowe.Api.Models;
using Crowe.Api.Tests.Helpers;
using Crowe.DataAccess.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Crowe.Api.Tests.MessagesControllerTests
{
    [TestClass]
    public class AddMessageTests : BaseMessagesControllerTests
    {
        protected List<IMessageWrapper> MessageList { get; set; } = new List<IMessageWrapper>();

        [TestMethod]
        public void HelloWorldMessageAdded()
        {
            Assert.IsFalse(this.MessageList.Any());
            this.MessagesController.AddMessage(this.HelloWorldMessage.Message);

            Assert.AreEqual(this.MessageList.Count, 1);
            Assert.IsTrue(this.MessageList.First().PropertiesEqual(this.HelloWorldMessage));
        }

        [TestMethod]
        public void TestMessageAdded()
        {
            Assert.IsFalse(this.MessageList.Any());
            this.MessagesController.AddMessage(this.TestMessage.Message);

            Assert.AreEqual(this.MessageList.Count, 1);
            Assert.IsTrue(this.MessageList.First().PropertiesEqual(this.TestMessage));
        }

        protected override void SetupMock(Mock<IDataAccessService> mockObject)
        {
            mockObject.Setup(m => m.AddMessage(this.HelloWorldMessage.Message))
                .Callback(() => this.MessageList.Add(this.HelloWorldMessage));

            mockObject.Setup(m => m.AddMessage(this.TestMessage.Message))
                .Callback(() => this.MessageList.Add(this.TestMessage));
        }
    }
}
