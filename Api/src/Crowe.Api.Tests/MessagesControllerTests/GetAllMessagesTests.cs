﻿using System.Collections.Generic;
using System.Linq;
using Crowe.Api.Models;
using Crowe.Api.Tests.Helpers;
using Crowe.DataAccess.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Crowe.Api.Tests.MessagesControllerTests
{
    [TestClass]
    public class GetAllMessagesTests : BaseMessagesControllerTests
    {
        private IEnumerable<IMessageWrapper> AllMessages => new List<IMessageWrapper>
        {
            this.HelloWorldMessage,
            this.TestMessage
        };

        [TestMethod]
        public void GetsAllMessages()
        {
            var messagesResponse = this.MessagesController.GetAllMessages();
            var messages = messagesResponse.ToObject<IEnumerable<MessageWrapper>>();
            var messagesList = messages.ToList();
            var allMessagesList = this.AllMessages.ToList();

            Assert.AreEqual(messagesList.Count, allMessagesList.Count);

            for (var i = 0; i < messagesList.Count; i++)
            {
                Assert.IsTrue(messagesList[i].PropertiesEqual(allMessagesList[i]));
            }
        }

        [TestMethod]
        public void GetHelloWorldMessage()
        {
            var response = this.MessagesController.GetAllMessages(this.HelloWorldMessage.Message);
            var responseList = response.ToObject<List<MessageWrapper>>();
            Assert.AreEqual(responseList.Count, 1);
            Assert.IsTrue(responseList.First().PropertiesEqual(this.HelloWorldMessage));
        }

        protected override void SetupMock(Mock<IDataAccessService> mockObject)
        {
            mockObject.Setup(m => m.GetAllMessages<IMessageWrapper>(null)).Returns(this.AllMessages);
            mockObject.Setup(m => m.GetAllMessages<IMessageWrapper>(this.HelloWorldMessage.Message)).Returns(
                new List<IMessageWrapper>
                    {this.HelloWorldMessage});
        }
    }
}
