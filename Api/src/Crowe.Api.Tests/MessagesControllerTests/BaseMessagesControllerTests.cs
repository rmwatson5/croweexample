﻿using System;
using Crowe.Api.Controllers;
using Crowe.Api.Models;
using Crowe.DataAccess.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Crowe.Api.Tests.MessagesControllerTests
{
    
    public abstract class BaseMessagesControllerTests
    {
        protected IMessageWrapper HelloWorldMessage => new MessageWrapper
            { Id = 0, Message = "Hello World", Timestamp = DateTime.Now };

        protected IMessageWrapper TestMessage => new MessageWrapper
            { Id = 1, Message = "Test", Timestamp = DateTime.Now.AddDays(-1) };

        protected MessagesController MessagesController;

        [TestInitialize]
        public void Initialize()
        {
            var mockObject = new Mock<IDataAccessService>();
            this.SetupMock(mockObject);
            this.MessagesController = new MessagesController(mockObject.Object);
        }

        protected abstract void SetupMock(Mock<IDataAccessService> mockObject);
    }
}
