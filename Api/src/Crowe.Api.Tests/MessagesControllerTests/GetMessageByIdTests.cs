﻿using System.Collections.Generic;
using System.Linq;
using Crowe.Api.Models;
using Crowe.Api.Tests.Helpers;
using Crowe.DataAccess.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Crowe.Api.Tests.MessagesControllerTests
{
    [TestClass]
    public class GetMessageByIdTests : BaseMessagesControllerTests
    {
        [TestMethod]
        public void GetHelloWorldMessageById()
        {
            var helloWorldMessageResult = this.MessagesController.GetMessageById(this.HelloWorldMessage.Id);
            var helloWorldMessage = helloWorldMessageResult.ToObject<MessageWrapper>();

            Assert.IsTrue(helloWorldMessage.PropertiesEqual(this.HelloWorldMessage));
        }

        [TestMethod]
        public void GetTestMessageById()
        {
            var testMessageResult = this.MessagesController.GetMessageById(this.TestMessage.Id);
            var testMessage = testMessageResult.ToObject<MessageWrapper>();

            Assert.IsTrue(testMessage.PropertiesEqual(this.TestMessage));
        }

        protected override void SetupMock(Mock<IDataAccessService> mockObject)
        {
            mockObject.Setup(m => m.GetMessageById<IMessageWrapper>(this.HelloWorldMessage.Id))
                .Returns(this.HelloWorldMessage);

            mockObject.Setup(m => m.GetMessageById<IMessageWrapper>(this.TestMessage.Id))
                .Returns(this.TestMessage);
        }
    }
}
