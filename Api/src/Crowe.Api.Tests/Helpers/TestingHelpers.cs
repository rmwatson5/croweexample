﻿namespace Crowe.Api.Tests.Helpers
{
    public static class TestingHelpers
    {
        public static bool PropertiesEqual<T>(this T item, T comparer)
        {
            if (item == null)
            {
                return comparer == null;
            }

            if (comparer == null)
            {
                return false;
            }

            var result = true;
            var properties = item.GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(item);
                var comparerValue = propertyInfo.GetValue(comparer);

               

                result = result && value.ValueEqual(comparerValue);
            }

            return result;
        }

        private static bool ValueEqual(this System.Object value, System.Object comparerValue)
        {
            if (value is System.DateTime valueDate && comparerValue is System.DateTime comparerValueDate)
            {
                return valueDate.Year == comparerValueDate.Year && valueDate.Month == comparerValueDate.Month && valueDate.Day == comparerValueDate.Day;
            }

            return value == comparerValue || value.Equals(comparerValue);
        }
    }
}
