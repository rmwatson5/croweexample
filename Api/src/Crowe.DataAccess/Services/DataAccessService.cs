﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace Crowe.DataAccess.Services
{
    public class DataAccessService : IDataAccessService
    {
        protected readonly IMapper Mapper;
        public DataAccessService(IMapper mapper)
        {
            this.Mapper = mapper;
        }

        public IEnumerable<TMessage> GetAllMessages<TMessage>(string message)
        {
            using (var context = new CroweExampleEntities())
            {
                var messages = context.DaMessageWrappers.AsQueryable();
                if (message != null)
                {
                    messages = messages.Where(m => m.Message == message);
                }

                var messagesList = messages.ToList();
                return this.Mapper.Map<IEnumerable<TMessage>>(messagesList);
            }
        }

        public TMessage GetMessageById<TMessage>(int id)
        {
            using (var context = new CroweExampleEntities())
            {
                var messages = context.DaMessageWrappers.AsQueryable();
                messages = messages.Where(m => m.Id == id).Take(1);

                var messagesList = messages.ToList().FirstOrDefault();
                return this.Mapper.Map<TMessage>(messagesList);
            }
        }

        public void AddMessage(string message)
        {
            using (var context = new CroweExampleEntities())
            {
                var messageWrapper = new DaMessageWrapper
                {
                    Message = message,
                    Timestamp = DateTime.Now
                };

                context.DaMessageWrappers.Add(messageWrapper);
                context.SaveChanges();
            }
        }
    }
}
