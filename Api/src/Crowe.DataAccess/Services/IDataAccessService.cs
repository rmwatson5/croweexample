﻿using System.Collections.Generic;

namespace Crowe.DataAccess.Services
{
    public interface IDataAccessService
    {
        /// <summary>
        /// Gets all messages.
        /// </summary>
        /// <param name="message"></param>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <returns></returns>
        IEnumerable<TMessage> GetAllMessages<TMessage>(string message);

        /// <summary>
        /// Gets the message by identifier.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        TMessage GetMessageById<TMessage>(int id);

        /// <summary>
        /// Adds the message.
        /// </summary>
        /// <param name="message">The message.</param>
        void AddMessage(string message);
    }
}