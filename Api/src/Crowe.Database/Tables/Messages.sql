﻿CREATE TABLE [dbo].[Messages]
(
	[Id] INT NOT NULL PRIMARY KEY identity(0,1), 
    [Message] VARCHAR(MAX) NOT NULL, 
    [Timestamp] DATETIME NOT NULL DEFAULT current_timestamp
)
