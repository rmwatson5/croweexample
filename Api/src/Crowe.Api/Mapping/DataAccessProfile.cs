﻿using AutoMapper;
using Crowe.Api.Models;
using Crowe.DataAccess;

namespace Crowe.Api.Mapping
{
    public class DataAccessProfile : Profile
    {
        protected override void Configure()
        {
            this.CreateMap<DaMessageWrapper, MessageWrapper>();
            this.CreateMap<DaMessageWrapper, IMessageWrapper>()
                .As<MessageWrapper>();
        }
    }
}
