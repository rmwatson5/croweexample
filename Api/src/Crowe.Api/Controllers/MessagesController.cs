﻿using System.Web.Mvc;
using Crowe.Api.Models;
using Crowe.DataAccess.Services;
using Newtonsoft.Json.Linq;

namespace Crowe.Api.Controllers
{
    public class MessagesController : Controller
    {
        protected readonly IDataAccessService DataAccessService;

        public MessagesController(IDataAccessService dataAccessService)
        {
            DataAccessService = dataAccessService;
        }

        [HttpGet]
        public JArray GetAllMessages(string message = null)
        {
            var messages = this.DataAccessService.GetAllMessages<IMessageWrapper>(message);
            return messages != null ? JArray.FromObject(messages) : null;
        }

        [HttpGet]
        public JObject GetMessageById(int id)
        {
            var message = this.DataAccessService.GetMessageById<IMessageWrapper>(id);
            return message != null ? JObject.FromObject(message) : null;
        }

        [HttpPost]
        public void AddMessage(string message)
        {
            this.DataAccessService.AddMessage(message);
        }
    }
}