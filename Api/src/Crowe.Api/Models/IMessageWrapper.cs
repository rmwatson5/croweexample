﻿using System;

namespace Crowe.Api.Models
{
    public interface IMessageWrapper
    {
        int Id { get; set; }
        string Message { get; set; }
        DateTime Timestamp { get; set; }
    }
}