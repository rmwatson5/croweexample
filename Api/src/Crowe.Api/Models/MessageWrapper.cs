﻿using System;
using Newtonsoft.Json;

namespace Crowe.Api.Models
{
    public class MessageWrapper : IMessageWrapper
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
    }
}