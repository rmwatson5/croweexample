﻿using System.Web.Mvc;
using AutoMapper;
using SimpleInjector;
using Crowe.Api;
using Crowe.Api.Mapping;
using Crowe.DataAccess.Services;
using SimpleInjector.Integration.Web.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(DependencyInjectionInitializer), "Initialize", Order = 100)]
namespace Crowe.Api
{
    public static class DependencyInjectionInitializer
    {
        public static void Initialize()
        {
            var container = new Container();

            // Services
            container.RegisterSingleton<IDataAccessService, DataAccessService>();

            // Mapping Profiles
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DataAccessProfile>();
            });

            mapperConfiguration.AssertConfigurationIsValid();
            container.Register(typeof(IMapper), () => mapperConfiguration.CreateMapper(), Lifestyle.Singleton);

            container.RegisterMvcIntegratedFilterProvider();
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}