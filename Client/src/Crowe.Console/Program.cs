﻿using System.IO;
using Crowe.Console.Models;
using Crowe.WebManager;
using Crowe.WebManager.Models;
using Microsoft.Extensions.Configuration;

namespace Crowe.Console
{
    class Program
    {
        private const string AppConfiguration = "AppConfiguration";
        private const string BaseUrl = "baseUrl";
        private const string GetMessageById = "getMessageById";

        static void Main(string[] args)
        {
            var configuration = GetConfigurationRoot();
            var appConfiguration = configuration.GetSection(AppConfiguration);
            var url = $"{appConfiguration[BaseUrl]}/{appConfiguration[GetMessageById]}?id=0";

            var dataService = new DataService();
            var requestWrapper = new RequestWrapper
            {
                RequestType = RequestType.Get,
                Url = url
            };

            var response = dataService.GetWebResponse<MessageWrapper>(requestWrapper);

            System.Console.WriteLine(response != null ? response.Message : "Hello World!");
        }

        static IConfigurationRoot GetConfigurationRoot()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            return builder.Build();
        }
    }
}
