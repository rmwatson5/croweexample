﻿using System;
using Newtonsoft.Json;

namespace Crowe.Console.Models
{
    public class MessageWrapper
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
    }
}
