﻿using System.Collections.Generic;

namespace Crowe.WebManager.Models
{
    public class RequestWrapper
    {
        public string Url;
        public RequestType RequestType;
        public object RequestPayload;
        public Dictionary<string, string> Headers = new Dictionary<string, string>();
    }
}
