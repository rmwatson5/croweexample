﻿namespace Crowe.WebManager.Models
{
    public enum RequestType
    {
        Get,
        Post
    }
}
