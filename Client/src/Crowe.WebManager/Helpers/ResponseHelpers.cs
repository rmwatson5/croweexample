﻿using Newtonsoft.Json;

namespace Crowe.WebManager.Helpers
{
    public static class ResponseHelpers
    {
        public static JsonSerializerSettings WebServiceSerializerSettings => new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
            TypeNameHandling = TypeNameHandling.None,
            NullValueHandling = NullValueHandling.Ignore
        };
    }
}
