﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using Crowe.WebManager.Helpers;
using Crowe.WebManager.Models;
using Newtonsoft.Json;

namespace Crowe.WebManager
{
    public class DataService
    {
        public TResponse GetWebResponse<TResponse>(RequestWrapper requestParameters)
        {
            var response = this.GetWebResponse(requestParameters);
            return JsonConvert.DeserializeObject<TResponse>(response);
        }

        protected string GetWebResponse(RequestWrapper requestParamaters)
        {
            var responseString = string.Empty;
            var serviceRequest = this.GetWebRequest(requestParamaters);

            try
            {
                var serviceResponse = serviceRequest.GetResponse();
                using (var responseStream = serviceResponse.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        var responseReader = new StreamReader(responseStream);
                        responseString = responseReader.ReadToEnd();
                        responseStream.Close();
                    }
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            var message = reader.ReadToEnd();
                            return message;
                        }
                    }
                }
            }

            return responseString;
        }

        protected HttpWebRequest GetWebRequest(RequestWrapper requestParamaters)
        {
            const string Get = "GET";
            const string Post = "POST";

            var serviceRequest = (HttpWebRequest)WebRequest.Create(requestParamaters.Url);

            if (requestParamaters.Headers != null && requestParamaters.Headers.Any())
            {
                requestParamaters.Headers.ToList().ForEach(kv => serviceRequest.Headers.Add(kv.Key, kv.Value));
            }

            switch (requestParamaters.RequestType)
            {
                case RequestType.Get:
                    serviceRequest.Method = Get;
                    break;
                case RequestType.Post:
                    serviceRequest.Method = Post;
                    this.SetRequestData(requestParamaters, serviceRequest);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return serviceRequest;
        }

        protected void SetRequestData(RequestWrapper requestParamaters, HttpWebRequest serviceRequest)
        {
            // Serialize the request data and send it over
            if (requestParamaters.RequestPayload == null)
            {
                return;
            }

            serviceRequest.Accept = "application/json";
            serviceRequest.ContentType = "application/json";
            var serializedRequest = JsonConvert.SerializeObject(requestParamaters.RequestPayload, ResponseHelpers.WebServiceSerializerSettings);
            serviceRequest.ContentLength = serializedRequest.Length;

            using (var streamWriter = new StreamWriter(serviceRequest.GetRequestStream()))
            {
                streamWriter.Write(serializedRequest);
                streamWriter.Flush();
            }
        }
    }
}
