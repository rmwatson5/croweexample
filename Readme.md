# Crowe Example

The following is a code example for a basic Api call from a .NET Core console application to a Api communicating with SQL server using Linq to Entity.

# Client projects

  - Console application (.NET Core)
  - Web Manager: Makes the api call (.NET Core)

# Server projects

  - API: .NET MVC Application that serves as the main point of entry
  - API Tests: .NET Class library for unit testing the API project
  - Data Access: .NET Class library for communicating to SQL server using Linq to SQL
  - Database: Data-Tier project for creating the database schema

### Technologies Used

Below is a list of NuGet Projects I imported:

* [AutoMapper] - A convention-based object-object mapper
* [EntityFramework] - Microsoft's recommended data access technology for new applications
* [Microsoft.AspNet.Mvc] - For the API
* [Newtonsoft.Json] - For serializing and deserializing requests
* [SimpleInjector] - For dependency injection
* [WebActivatorEx] - allows other packages to execute some startup code in web apps


   [AutoMapper]: <http://automapper.org>
   [EntityFramework]: <https://github.com/aspnet/EntityFramework6>
   [Microsoft.AspNet.Mvc]: <https://www.asp.net/mvc>
   [Newtonsoft.Json]: <https://www.newtonsoft.com/json>
   [SimpleInjector]: <https://simpleinjector.org/index.html>
   [WebActivatorEx]: <https://github.com/davidebbo/WebActivator>
